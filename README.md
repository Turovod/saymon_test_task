# saymon_autotest

## Getting started

- [ ] Открыть доступ (Linux, macOs): chmod +x ./selenoid_linux
- [ ] Запустить селеноид: ./selenoid_linux -conf ./browsers.json
- [ ] Запустить селеноид: ./selenoid_windows.exe ./browsers.json

## Roadmap

- [ ] Доделать проверку спиннера
- [ ] Логировать не залогированное
- [ ] Аллюрить не заалюрренное
- [ ] Дописать тест
- [ ] Написать декоратьор @retry

## Allure

- [ ] Для работы аллюра нужна джава
  - Запуск аллюра: python -m pytest --alluredir allure-results
  - Запуск отчета: allure serve allure-result
