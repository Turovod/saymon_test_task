import allure
from selenium.webdriver.common.by import By

from src.pages.operation_page import OperationPage
from src.elements.base_element import BaseElement

TEST_OPERATION_NAME = "Операция1"


@allure.epic('Страница "Операции"')
@allure.feature('Выполнение операции')
@allure.suite('Тесты из раздела "Операции"')
@allure.tag('Операция', 'Выполнение операции')
@allure.testcase("https://docs.yandex.ru/docs/view?url=ya-disk-public%3A%2F%2Ffk2AHWjERKo1b66mIpIP4PH7SQFVinp%2BmX"
                 "6xJXO8zyFz2tuhuN7fv0N89wyZTFzA%2FCH%2B%2BsnE5duAiqM%2FEjDILQ%3D%3D&name=%D0%A2%D0%B5%D1%81%D1%82%D0%B"
                 "E%D0%B2%D0%BE%D0%B5%20%D0%B7%D0%B0%D0%B4%D0%B0%D0%BD%D0%B8%D0%B5%20(%D0%B8%D0%BD%D0%B6%D0%B5%D0%BD%D"
                 "0%B5%D1%80%20%D0%BF%D0%BE%20%D0%B0%D0%B2%D1%82%D0%BE%D0%BC%D0%B0%D1%82%D0%B8%D0%B7%D0%B0%D1%86%D0%B8"
                 "%D0%B8%20%D1%82%D0%B5%D1%81%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D1%8F).docx&nosw=1",
                 "Выполнение операции")
class TestOperation:
    @allure.title("Вкладка 'Операции'")
    @allure.sub_suite('Операции')
    def test_operation(self, selenium_driver):
        op = OperationPage(selenium_driver)

        with allure.step('1. Открыть страницу операций'):
            op.open_operation_page()
        with allure.step('2. Авторизоваться'):
            op.login()
        with allure.step('3. Проскроллить список опираций до конца'):
            op.scroll_operations_history(TEST_OPERATION_NAME)
        with allure.step(f'4. Посчитать количество выполненых операций с именем {TEST_OPERATION_NAME}'):
            initial_count_operations = len(op.get_all_operations_with_name(name=TEST_OPERATION_NAME))
        with allure.step(f'5. Выполнить операцию с именем  {TEST_OPERATION_NAME}'):
            op.perform_operation_with_name(TEST_OPERATION_NAME)
        with allure.step(f'6. Посчитать количество выполненых операций с именем {TEST_OPERATION_NAME}'):
            final_count_operations = len(op.get_all_operations_with_name(name=TEST_OPERATION_NAME))

        assert initial_count_operations + 1 == final_count_operations, \
            f'Количество сделок {final_count_operations} не соответствует расчетному {initial_count_operations + 1}'

    @allure.title("Вкладка 'Операции'")
    @allure.sub_suite('Операции')
    def test_check_user_results(self, selenium_driver):
        OPERATION_DATA = {'user': 'aqa', 'stdout': 'Test'}
        op = OperationPage(selenium_driver)

        with allure.step('1. Открыть страницу операций'):
            op.open_operation_page()
        with allure.step('2. Авторизоваться'):
            op.login()
        with allure.step(f'3. Выполнить операцию с именем  {TEST_OPERATION_NAME}'):
            op.perform_operation_with_name(TEST_OPERATION_NAME)
        with allure.step('4. Получить юзера и результат выполненной сделки'):
            user = op.get_last_user_trigger()
            result = eval(op.get_last_result_operation())

        assert user == OPERATION_DATA['user'], f'Имя юзера {user} не совпадает с ожидаемым {OPERATION_DATA["user"]}'
        assert result["stdout"] == OPERATION_DATA[
            'stdout'], f'{result["stdout"]} не совпадает {OPERATION_DATA["stdout"]}'
