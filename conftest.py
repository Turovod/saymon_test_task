import logging.config
from os import path

logging_ini = path.join(path.dirname(path.abspath(__file__)), 'logging.ini')
logging.config.fileConfig(logging_ini)

pytest_plugins = [
    "src.fixtures"
]


def pytest_addoption(parser):
    parser.addini('selenium_url', 'Selenium host url')
    parser.addini('browser_name', 'Browser name')
    parser.addini('browser_version', 'Browser version')
    parser.addini('headless', 'Headless mode')
    parser.addini('disable_gpu', 'Disable gpu mode')
    parser.addini('implicitly_wait', 'Implicitly wait')
