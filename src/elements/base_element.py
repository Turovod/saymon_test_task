from __future__ import annotations
import logging
from selenium.webdriver.remote import webelement

from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.common.exceptions import NoSuchElementException, TimeoutException


class BaseElement:
    def __init__(self,
                 driver,
                 locator: tuple,
                 timeout: int | float = 15,
                 find_all: bool = False,
                 visible: bool = True):
        self.driver = driver
        self.wait = WebDriverWait(driver, timeout if visible else 3)
        self.locator = locator
        self.is_list = find_all
        self.e = self.find(find_all=find_all)

    def find(self, find_all: bool):
        try:
            if find_all:
                element = self.wait.until(ec.presence_of_all_elements_located(self.locator))
            else:
                element = self.wait.until(ec.presence_of_element_located(self.locator))
            if element:
                return element
        except (NoSuchElementException, TimeoutException):
            raise NoSuchElementException(f"Не удалось обнаружить элемент {self.locator}")

    def get_element(self, index: int = None) -> webelement:
        return self.e[index] if self.is_list else self.e

    def is_visible(self) -> bool:
        logging.debug("Проверка, что элемент 'is_visible'")
        try:
            return True if self.wait.until(ec.visibility_of(self.get_element())) else False
        except TimeoutException:
            return False

    def is_invisible(self) -> bool:
        logging.debug("Проверка, что элемент 'is_invisible'")
        try:
            return True if self.wait.until(ec.invisibility_of_element(self.get_element())) else False
        except TimeoutException:
            return True

    def is_disappeared(self) -> bool:
        try:
            self.wait.until_not(ec.presence_of_element_located(self.locator))
            logging.debug("Элемент исчез")
            return True
        except TimeoutException:
            logging.debug("Элемент не исчез")
            return False
        except NoSuchElementException:
            logging.debug("Элемент исчез")
            return True
