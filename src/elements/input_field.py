from __future__ import annotations
from src.elements.base_element import BaseElement


class InputField(BaseElement):
    def __init__(self,
                 driver,
                 locator: tuple,
                 timeout: int | float = 15,
                 find_all: bool = False,
                 visible: bool = True):
        super().__init__(driver, locator, timeout, find_all, visible)

    def fill(self, content: str):
        self.e.send_keys(str(content))
        # self.input_fill(content)
