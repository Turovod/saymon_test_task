from src.elements.base_element import BaseElement


class Button(BaseElement):
    def __init__(self, driver, locator):
        super().__init__(driver, locator)

    def click(self):
        self.e.click()
