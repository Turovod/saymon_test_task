from selenium.webdriver.common.by import By


class BaseLocators:
    BASE_URL = 'https://saas.saymon.info'

    SPINNER = (By.CSS_SELECTOR, '.loader')

    LOGIN_FIELD = (By.CSS_SELECTOR, '#user-login')
    PASSWORD_FIELD = (By.CSS_SELECTOR, '#user-password')
    SUBMIT_BTN = (By.CSS_SELECTOR, 'button[data-loading-text = "Login..."]')

    DROPDOWN_MENU_BTN = (By.CSS_SELECTOR, '.dropdown > button')
    LOGOUT_ITM = (By.CSS_SELECTOR, 'a[href="#/logout"]')