import os
from selenium.webdriver.common.by import By
from src.locators.base_locators import BaseLocators


class OperationLocators(BaseLocators):
    def get_operation_page_url(self):
        return os.path.join(BaseLocators.BASE_URL, '#/objects/6643411ab317dd06b2dc9a75/end-view')

    def get_operation_btn_by_name(self, name):
        return (By.XPATH, f'//td[contains(text(), "{name}")]/following-sibling::td')

    def get_operation_locator_by_name(self, name):
        return (By.XPATH, f'//div[@id="operations-history"]//span[contains(text(), "{name}")]')

    def get_last_user_trigger_locator(self):
        return (By.CSS_SELECTOR, 'span.js-data-cell.js-user-cell')

    def get_last_result_locator(self):
        return (By.CSS_SELECTOR, '.description > span')
