import os

from selenium import webdriver
import pytest
import logging

from src.pages.base_page import BasePage


# @pytest.fixture
# def selenium_driver():
#     logging.debug(f"#Получение драйвера")
#     if os.name == 'nt':
#         executable_path = 'drivers/chromedriver.exe'
#     if os.name == 'posix':
#         executable_path = 'drivers/chromedriver_linux'
#     webdriver.ChromeService(executable_path=executable_path)
#     driver = webdriver.Chrome()
#
#     yield driver
#
#     logging.debug(f"#Завершение сессии")
#     BasePage(driver).logoff()
#     driver.quit()

@pytest.fixture
def selenium_driver(request, pytestconfig):
    logging.info('Prepare browser...')
    chrome_options = webdriver.ChromeOptions()
    if pytestconfig.getini('headless') is True:
        chrome_options.add_argument('--headless')
    if pytestconfig.getini('disable_gpu') is True:
        chrome_options.add_argument('--disable-gpu')
    chrome_options.set_capability('browserName', pytestconfig.getini('browser_name'))
    chrome_options.set_capability('browserVersion', pytestconfig.getini('browser_version'))
    chrome_options.page_load_strategy = 'normal'

    driver = webdriver.Remote(command_executor=pytestconfig.getini('selenium_url'), options=chrome_options)
    waiting_time = int(pytestconfig.getini('implicitly_wait'))
    if waiting_time > 0:
        driver.implicitly_wait(waiting_time)
    driver.maximize_window()

    logging.info('Browser was started...')

    yield driver

    BasePage(driver).logoff()
    driver.quit()
