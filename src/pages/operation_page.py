import time

import allure

from src.elements.base_element import BaseElement
from src.elements.button import Button
from src.locators.operation_locators import OperationLocators
from selenium.webdriver import ActionChains, Keys

from src.pages.base_page import BasePage


class OperationPage(BasePage):

    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

    @allure.step("Переход на страницу операций")
    def open_operation_page(self):
        operation_url = OperationLocators().get_operation_page_url()
        self.open_page(operation_url)

    @allure.step(f"Выполнение операции")
    def perform_operation_with_name(self, name):
        operation_btn = OperationLocators().get_operation_btn_by_name(name)
        Button(self.driver, operation_btn).click()
        time.sleep(0.2)

    @allure.step(f"Получение полного списка операций по имени")
    def get_all_operations_with_name(self, name):
        operation_locator = OperationLocators().get_operation_locator_by_name(name)
        list_elements = BaseElement(driver=self.driver, locator=operation_locator, find_all=True).e
        list_elements[0].click()
        return list_elements

    @allure.step(f"Скроллинг списка операций вниз")
    def scroll_operations_history(self, operation_name):
        initial_number_operations = len(self.get_all_operations_with_name(name=operation_name))
        current_number_operations = 0

        while initial_number_operations != current_number_operations:
            ActionChains(self.driver).key_down(Keys.LEFT_CONTROL).send_keys(Keys.END).perform()
            initial_number_operations = current_number_operations
            time.sleep(0.4)
            current_number_operations = len(self.get_all_operations_with_name(name=operation_name))
            time.sleep(0.4)

    @allure.step(f"Получение последнего юзера\триггера")
    def get_last_user_trigger(self):
        # return self.get_text_from_element(OperationLocators().get_last_user_trigger_locator())
        return BaseElement(driver=self.driver, locator=OperationLocators().get_last_user_trigger_locator()).e.text

    @allure.step(f"Получение последнего результата")
    def get_last_result_operation(self):
        return self.get_text_from_element(OperationLocators().get_last_result_locator())
        # return BaseElement(driver=self.driver, locator=OperationLocators().get_last_result_locator()).e.text
