import os

import allure
import logging

from selenium.common import NoSuchElementException, StaleElementReferenceException

from src.elements.button import Button
from src.elements.input_field import InputField
from src.locators.base_locators import BaseLocators
from src.elements.base_element import BaseElement

from dotenv import load_dotenv


class BasePage:

    def __init__(self, driver):
        self.driver = driver
        self.page_elements = {}

    @allure.step("Переход по URL")
    def get(self, url: str, with_delete_cookies: bool = False) -> None:
        if with_delete_cookies:
            self.driver.delete_all_cookies()
        self.driver.get(url)

    def assert_page(self, url) -> None:
        logging.debug(f"#Проверка url открытой страницы: {url}")
        assert self.__get_url(clear_query=True) == url

    def __get_url(self, clear_query: bool = False) -> str:
        logging.debug(f"#Получение url открытой страницы: {self.driver.current_url}")
        url = self.driver.current_url
        return url if clear_query else url.split("?")[0]

    @allure.step("Проверка отработки спиннера")
    def validate_loading_spinner_disappeared(self, timeout: int = 35, locator: tuple = BaseLocators.SPINNER):
        """
            Метод ожидает исчезновения спиннера загрузки
        """
        logging.debug("Проверка, что спиннер загрузки контента на странице исчез")
        try:
            loading_wheel = BaseElement(self.driver, locator, timeout=2).is_visible()
            if loading_wheel:
                try:
                    result = BaseElement(self.driver, locator, timeout=timeout).is_invisible()
                    if not result:
                        assert False, 'Спиннер загрузки контента не исчез'
                    else:
                        logging.debug("#Спиннер загрузки контента исчез")
                except AssertionError as error:
                    logging.error(error)
                    raise
            else:
                logging.debug("#Спиннер загрузки контента на странице не виден")
        except (NoSuchElementException, StaleElementReferenceException):
            logging.debug("#Спиннер загрузки контента на странице не виден")

    def open_page(self, url: str, with_delete_cookies: bool = False) -> bool:
        logging.debug(f"#Открытие страницы: {url}")
        self.get(url=url, with_delete_cookies=with_delete_cookies)
        self.validate_loading_spinner_disappeared(locator=BaseLocators.SPINNER)
        # self.assert_page(url=url)
        return True

    def login(self):
        load_dotenv()
        LOGIN = os.getenv("LOGIN")
        PASSWORD = os.getenv("PASSWORD")

        InputField(self.driver, BaseLocators.LOGIN_FIELD).fill(LOGIN)
        InputField(self.driver, BaseLocators.PASSWORD_FIELD).fill(PASSWORD)
        Button(self.driver, BaseLocators.SUBMIT_BTN).click()

    def logoff(self):
        Button(self.driver, BaseLocators.DROPDOWN_MENU_BTN).click()
        Button(self.driver, BaseLocators.LOGOUT_ITM).click()

    def get_text_from_element(self, locator):
        return BaseElement(driver=self.driver, locator=locator).e.text
