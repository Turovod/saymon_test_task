from selenium.webdriver.common.by import By


class CountElements:

    def get_count_elements_contains_text(self, driver, text, element_tag='*'):
        return len(driver.find_elements(By.XPATH, f'//{element_tag}[contains(text(), {text})]'))
